package com.example.animations_demo.presentation.navigation

import androidx.compose.runtime.Composable
import com.example.animations_demo.presentation.component.AnimationsDemoScreen

class AnimationsDemoNavigation {
    @Composable
    fun EntryPoint() {
        AnimationsDemoScreen()
    }
}